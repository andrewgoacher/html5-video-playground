# video-player

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

To create the mp4s I had to run:

```
> ./ffmpeg.exe -i 'inputfile.mp4' -movflags frag_keyframe+empty_moov+default_base_moof 'outputfile.mp4'
```
