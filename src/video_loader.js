class Loader {
    static async Load(data) {
        // eslint-disable-next-line
        return new Promise((resolve, reject) => {
            const media = document.createElement("video");
            const blobUrl = URL.createObjectURL(new Blob([new Uint8Array(data)]));
            media.addEventListener('loadedmetadata', async () => {
                resolve({
                    duration: media.duration,
                    buffer: data
                });
            });
            media.src = blobUrl;
        });
    }

    constructor() {
        this.media_source = new MediaSource();
        this.buffer = null;
        this.instance = null;
        this.media_chunk = null;
    }

    async attach(videoElement, onupdateend) {
        // eslint-disable-next-line
        return new Promise((resolve, reject) => {
            this.instance = videoElement;
            const src = URL.createObjectURL(this.media_source);
            this.media_source.addEventListener('sourceopen', () => {
                this.buffer = this.media_source.addSourceBuffer(mime_and_codecs.webm);
                this.buffer.addEventListener('updateend', () => {

                    this.buffer.timestampOffset += this.media_chunk.duration;
                    onupdateend();
                });
            });
            this.instance.src = src;
            resolve();
        });
    }

    createUrl(video, part) {
        return `http://localhost:8086/stream/${video}/${part}`;
    }

    async appendToBuffer(video, part) {
        // eslint-disable-next-line
        return new Promise(async (resolve, reject) => {
            const url = this.createUrl(video, part);
            let response = await fetch(url);
            let arrayBuffer = await response.arrayBuffer();
            const media = await Loader.Load(arrayBuffer);
            this.media_chunk = media;
            this.buffer.appendBuffer(media.buffer);
            resolve();
        })
    }
}

const mime_and_codecs = {
    webm: 'video/webm; codecs="vorbis,vp8"'
};

export default Loader;
export { mime_and_codecs };